import React from 'react'
import classes from './Memory.scss'

//#memorycards
const cards = 16

export const Memory = (props) => (
  <div>
    {/* <h2 className={classes.memoryContainer}>
      Memory:
      {' '}
      <span className={classes['memory--red']}>
        {props.memory}
      </span>
    </h2>
    <button className='btn btn-default' onClick={props.increment}>
      Increment
    </button>
    {' '}
    <button className='btn btn-default' onClick={props.doubleAsync}>
      Double (Async)
    </button> */}
    
      <div className={classes['memory-card-closed']}>?</div>

  </div>
)

Memory.propTypes = {
  memory: React.PropTypes.number.isRequired,
  doubleAsync: React.PropTypes.func.isRequired,
  increment: React.PropTypes.func.isRequired
}

export default Memory
