import React from 'react'
import classes from './MyCounter.scss'

export const MyCounter = (props) => (
  <div>
    <h2 className={classes.mycounterContainer}>
      MyCounter:
      {' '}
      <span className={classes['counter--red']}>
        {props.mycounter}
      </span>
    </h2>
    <button className='btn btn-default' onClick={props.increment}>
      Increment
    </button>
    {' '}
    <button className='btn btn-default' onClick={props.doubleAsync}>
      Double (Async)
    </button>
  </div>
)

MyCounter.propTypes = {
  mycounter: React.PropTypes.number.isRequired,
  doubleAsync: React.PropTypes.func.isRequired,
  increment: React.PropTypes.func.isRequired
}

export default MyCounter
