import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'mycounter',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const MyCounter = require('./containers/MyCounterContainer').default
      const reducer = require('./modules/mycounter').default

      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'mycounter', reducer })

      /*  Return getComponent   */
      cb(null, MyCounter)

    /* Webpack named bundle   */
  }, 'mycounter')
  }
})
